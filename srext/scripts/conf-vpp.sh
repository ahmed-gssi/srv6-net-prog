set interface state GigabitEthernet0/8/0 up
set interface ip address GigabitEthernet0/8/0 1:2::1/64
set interface ip address GigabitEthernet0/8/0 12.0.0.1/24

create host-interface name vpp0
set interface state host-vpp0 up
set interface ip address host-vpp0 a::1/64

create host-interface name vpp1
set interface state host-vpp1 up
set interface ip address host-vpp1 10.0.0.1/24

create host-interface name vpp2
set interface state host-vpp2 up
set interface ip address host-vpp2 192.168.1.1/24

set sr encaps source addr 1:2::1
sr policy add bsid c1::1 next 2::ad6:f1 next 2::ad6:f2 next 3::d6 encap
sr steer l3 B::/64 via sr policy bsid c1::1

sr policy add bsid C1::2 next 2::ad4:f3 next 2::ad4:f4 next 3::d4 encap
sr steer l3 30.0.0.0/24 via sr policy bsid C1::2

sr policy add bsid C1::3 next 2:: next 2::c6 next 3::d2 encap
sr steer l2 host-vpp2 via sr policy bsid C1::3

ip route add 2::/64 via 1:2::2