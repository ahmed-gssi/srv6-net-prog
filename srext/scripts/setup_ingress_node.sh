
#!/bin/bash

# This script configures the ingress node for the SFC use-case
# It creates a network namespace that will be used as a client 
# It adds an SRv6 policy for traffic going to the server (namespace on egress node)
# It uses the srext kernel module to add an End.DX6 localsid
# The End.DX6 localsid is used to decapsulate the return traffic coming back from the server.. 
# .. before being sent to the client (namespace) 

#========================== creating client ==============================
./vnf-single_iface.sh add client veth0_1 inet6 A::1/64 A::2/64

#========================== Configuring SR policy =========================
sudo ip -6 route add B::/64 via 1:2::2 encap seg6 mode encap segs 2::AD6:F1,2::AD6:F2,2::AD6:F3,3::D6

#========================== Configuring localsid table =========================
sudo modprobe srext 
sudo srconf localsid add 1::d6 end.dx6 ip a::2 veth0_1

exit 
