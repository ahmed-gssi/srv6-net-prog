#!/bin/bash

# This script configures the NFV node for the SFC use-case
# It creates three network namespaces that are used as SR-unaware VNFS(SFs) 
# It uses the srext kernel module to add a dynamic proxy behavior, End.AD6 localsid, for each VNF 
# The End.AD6 localsids are used to handle the processing of SRv6 encapsulation ... 
# ... in behalf of the SR-unaware VNFs
# For simplicity, the End.AD6 (proxy behavior) will use the same interface as ...
# ... Target and source interface
# It uses the srext kernel module again to add an End localsid
# Then End localsid will be used by the return traffic from the server to the client 

#========================== creating VNFs ==============================
sudo ./vnf-single_iface.sh add vnf1 veth1_2 inet6 2:f1::1/64 2:f1::f1/64
sudo ./vnf-single_iface.sh add vnf2 veth2_2 inet6 2:f2::1/64 2:f2::f2/64
sudo ./vnf-single_iface.sh add vnf3 veth3_2 inet6 2:f3::1/64 2:f3::f3/64

#========================== Configuring localsid table =========================
sudo modprobe srext
sudo srconf localsid add 2::AD6:F1 end.ad6 ip 2:f1::f1 veth1_2 veth1_2 
sudo srconf localsid add 2::AD6:F2 end.ad6 ip 2:f2::f2 veth2_2 veth2_2 
sudo srconf localsid add 2::AD6:F3 end.ad6 ip 2:f3::f3 veth3_2 veth3_2 
sudo srconf localsid add 2:: end

exit 