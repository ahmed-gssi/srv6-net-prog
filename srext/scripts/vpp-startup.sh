
unix {
  nodaemon
  exec /vagrant/srext/scripts/conf-vpp.sh
  log /tmp/vpp.log
  full-coredump
  cli-listen /run/vpp/cli.sock
}

api-trace {
  on
}

cpu {

}

 dpdk {
	socket-mem 1024
	dev 0000:00:08.0
}
plugin_path /home/osboxes/vpp/build-root/install-vpp_debug-native/vpp/lib64/vpp_plugins
