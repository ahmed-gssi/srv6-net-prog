
#!/bin/bash

# This script configures the egress node for the SFC use-case
# It creates a network namespace that will be used as a server 
# It adds an SRv6 policy for return traffic going back from the server to the client
# It uses the srext kernel module to add an End.DX6 localsid
# The End.DX6 localsid is used to decapsulate the SRv6 before being sent to the server

#========================== creating client ==============================
sudo ./vnf-single_iface.sh add s0 veth0_3 inet6 B::1/64 B::2/64

#========================== Configuring SR policy for return traffic  =========================
sudo ip -6 route add A::/64 via 2:3::2 encap seg6 mode encap segs 2::,1::D6

#========================== Configuring localsid table =========================
sudo modprobe srext 
sudo srconf localsid add 3::d6 end.dx6 ip b::2 veth0_3

exit 