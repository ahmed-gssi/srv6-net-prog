#!/bin/bash

# This script is used by vagarnt for initial provisioning of NFV node

#=================== Install required softwares ================
export DEBIAN_FRONTEND=noninteractive
apt-get -y --force-yes install ethtool 
apt-get -y --force-yes install iperf 
apt-get -y --force-yes install iperf3 

#=============== enbling forwarding ===========================
sysctl -w net.ipv6.conf.all.forwarding=1

#===================== configuring interfaces ==================
ifconfig eth1 up
ip -6 addr add 1:2::2/64 dev eth1

ifconfig eth2 up
ip -6 addr add 2:3::2/64 dev eth2

#======================== Configuring routing ===================
ip -6 route add 1::/64 via 1:2::1
ip -6 route add 3::/64 via 2:3::3

#=========================== Installing SREXT =============================
git clone https://bitbucket.org/ahmed-gssi/srv6-net-prog
cd /home/sr6/srv6-net-prog/srext/
make && make install && depmod -a

exit
