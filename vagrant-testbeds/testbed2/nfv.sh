#!/bin/bash

# This script is used by vagrant for initial provisioning of NFV node

USER_HOME="/home/sr6"

#=================== Install required softwares ================
export DEBIAN_FRONTEND=noninteractive
apt-get -y --force-yes install ethtool 
apt-get -y --force-yes install iperf 
apt-get -y --force-yes install iperf3 

#=============== enbling forwarding ===========================
sysctl -w net.ipv4.conf.all.forwarding=1
sysctl -w net.ipv6.conf.all.forwarding=1

#===================== configuring interfaces ==================
ifconfig eth1 up
ip -6 addr add 1:2::2/64 dev eth1
ip addr add 12.0.0.2/24 dev eth1 

ifconfig eth2 up
ip -6 addr add 2:3::2/64 dev eth2
ip addr add 23.0.0.2/24 dev eth2 

#======================== Configuring routing ===================
ip -6 route add 1::/64 via 1:2::1
ip -6 route add 3::/64 via 2:3::3
ip route add 10.0.0.0/24 via 12.0.0.1
#=========================== Installing SREXT =============================
#cd /vagrant/srext/
cp -r /vagrant/srext $USER_HOME/
cd $USER_HOME/srext

sudo make && sudo make install && sudo depmod -a

cd $USER_HOME/srext/scripts/
./setup_nfv_node.sh

exit
