#!/bin/bash

# This script is used by vagarnt for initial provisioning of egress node

#=================== Install required softwares ================
export DEBIAN_FRONTEND=noninteractive
apt-get -y --force-yes install ethtool 
apt-get -y --force-yes install iperf 
apt-get -y --force-yes install iperf3 

#========================== Enble forwarding =========================
sysctl -w net.ipv4.conf.all.forwarding=1
sysctl -w net.ipv6.conf.all.forwarding=1

#=================== Configuring interfaces ==========================
ifconfig eth1 up
ip -6 addr add 2:3::3/64 dev eth1
ip addr add 23.0.0.3/24 dev eth1 
#======================== Configuring routing ======================
ip -6 route add 2::/64 via 2:3::2
ip route add 10.0.0.0/24 via 23.0.0.2
#=========================== Installing SREXT =============================
#cd /vagrant/srext/
cp -r /vagrant/srext $USER_HOME/
cd $USER_HOME/srext

sudo make && sudo make install && sudo depmod -a

cd $USER_HOME/srext/scripts/
./setup_egress_node.sh

exit
