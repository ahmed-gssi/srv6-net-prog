#!/bin/bash
#!/bin/bash

ip netns add c0
ip netns add c1
ip netns add c2

ip link add vpp0 type veth peer name veth-c0
ip link add vpp1 type veth peer name veth-c1
ip link add vpp2 type veth peer name veth-c2

ip link set vpp0 up
ip link set vpp1 up
ip link set vpp2 up

ip link set vpp0 promisc on
ip link set vpp1 promisc on
ip link set vpp2 promisc on

ip link set veth-c0 netns c0
ip link set veth-c1 netns c1
ip link set veth-c2 netns c2

ip netns exec c0 ip link set lo up
ip netns exec c1 ip link set lo up
ip netns exec c2 ip link set lo up

ip netns exec c0 ip link set veth-c0 up
ip netns exec c1 ip link set veth-c1 up
ip netns exec c2 ip link set veth-c2 up

ip netns exec c0 ip -6 addr add a::2/64 dev veth-c0
ip netns exec c1 ip addr add 10.0.0.2/24 dev veth-c1
ip netns exec c2 ip addr add 192.168.1.1/24 dev veth-c2

ethtool --offload  vpp0 rx off tx off
ethtool --offload  vpp1 rx off tx off
ethtool --offload  vpp2 rx off tx off

ip netns exec c0 ethtool --offload veth-c0 rx off tx off
ip netns exec c1 ethtool --offload veth-c1 rx off tx off
ip netns exec c2 ethtool --offload veth-c2 rx off tx off

ip netns exec c0 ip -6 route add default via a::1
ip netns exec c1 ip route add default via 10.0.0.1
ip netns exec c2 ip route add default via 192.168.1.1

sudo cp /vagrant/srext/scripts/vpp-startup.sh /etc/vpp/startup.conf
exit