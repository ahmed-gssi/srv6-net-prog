#!/bin/bash

# This script is used by vagarnt for initial provisioning of ingress node

USER_HOME="/home/osboxes"

#=================== Install required softwares ================
export DEBIAN_FRONTEND=noninteractive
apt-get -y --force-yes install ethtool
apt-get -y --force-yes install iperf
apt-get -y --force-yes install iperf3

#============================= Enble forwarding ===========================
sysctl -w net.ipv4.conf.all.forwarding=1
sysctl -w net.ipv6.conf.all.forwarding=1

#======================= Configuring interfaces ============================
ifconfig eth1 up
ip -6 addr add 1:2::1/64 dev eth1
#========================== Configuring routing ===========================
ip -6 route add 2::/64 via 1:2::2

#=========================== Installing SREXT =============================
#cd /vagrant/srext/
cp -r /vagrant/srext $USER_HOME/
cd $USER_HOME/srext

sudo make && sudo make install && depmod -a

exit
