# Vagrant/Virtualbox based testbed with Ingress(VPP) NFV and Egress 

### setup of the 3 VMs

Clone the srv6-net-prog repository in your machine:
```
$ git clone https://github.com/netgroup/SRv6-net-prog 

(private version on https://bitbucket.org/ssalsano/srv6-net-prog/)
$ cd srv6-net-prog
$ git checkout test

```

Go in testbed2 folder
```
$ cd vagrant-testbeds/testbed2 
```

Add the srext vagrant box:
```
$ vagrant box add sr-vnf http://cs.gssi.infn.it/files/SFC/sr_nfv_connector.box 
```
Add the vpp vagrant box, download vpp-vm.box from https://drive.google.com/drive/folders/0B11H-pzAF_BBMUo0bVlOM1BBNDA and then:
```
$ vagrant box add vpp-vm.box --name vpp-vm 
```
Run vagrant in the testbed2 folder:
```
$ vagrant up 
```

### Use of the testbed

Login in the VMs:
```
vagrant ssh ingress
vagrant ssh nfv
vagrant ssh egress
```
you can also ssh to the VMs using the port numbers that are shown in the vagrant console output,
the login/password are in the Vagratnfile:
```
ingress login: osboxes password: sr6
nfv     login: sr6     password: sr6
egress  login: sr6     password: sr6
```
In the ingress node, run vpp:
```
source vpp.sh &
```

For IPv6 traffic, run in the ingress node:
```
sudo ip netns exec c0 ping6 b::2
```
For IPv4 traffic, run in the ingress node:
```
sudo ip netns exec c1 ping 30.0.0.2
```
For L2 traffic, run in the ingress node:
```
sudo ip netns exec c2 ping6 192.168.1.2
```

Ahmed: The NFV and egress nodes are configured with the required SR behaviors "using srconf CLI"
For verification, you can use tcpdump as well as "srconf localsid show" command

Stefano: can you provide more details and example on how to use the tesbed and
some checks to be performed to verify the correct configuration of the nodes?


